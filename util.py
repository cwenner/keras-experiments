# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:expandtab:colorcolumn=80

__all__ = [
    'inf', 'nan',
    'div', 'divf',
    'iscallable', 'isdefined', 'isnan', 'isnull',
    'defaultto', 'listget', 'listset',
    'static_vars',
    'format_number',
    'numpy_to_list',
    'Interval', 'AttrDict',
]

import csv
import pandas as pd
import numpy as np
import math

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

inf = float('inf')
nan = float('nan')

# Zero-safe division.
# Uses numpy's divide operation while quenching warnings.
def div(x,y):
    olderr = np.seterr(all='ignore')
    z = np.divide(x,y)
    np.seterr(**olderr)
    return z

# Zero-safe float division.
def divf(x,y):
    return div(float(x), float(y))

def iscallable(x):
    return hasattr(x, '__call__')

# Not possible to check caller locals?
def isdefined(xname):
    return xname in globals() or xname in vars(__builtins__)

def isnan(x):
    return isinstance(x, float) and math.isnan(x)

def isnull(x):
    #or pd.isnull(x) # not safe
    if isnan(x) or (None is x):
        return True
    return False

def defaultto(x, y):
    return y if isnull(x) else x

def listget(x, i):
    return next(iter(x[i:]), None)

def listset(l, i, x):
    if i >= len(l):
        l.extend([None] * (len(l) - i + 1))
    l[i] = x

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

class Interval:
    def __init__(self, min_val=None, max_val=None):
        self.min_val = min_val
        self.max_val = max_val
    def __contains__(self, x):
        return (self.min_val is None or x >= self.min_val)\
            and (self.max_val is None or x <= self.max_val)
    def __str__(self):
        return unicode(self).encode('utf-8')
    def __unicode__(self):
        s = '['
        if self.min_val is not None:
            s += str(self.min_val)
        s += '..'
        if self.max_val is not None:
            s += str(self.max_val)
        return s + ']'

# Format a number to a given strict fixed width.
# Might not work with 
# `width` should be at least 6.
def format_number(x, width=6):
    if x == inf:
        return (" %%%ds" % width) % '+inf'
    elif x == -inf:
        return (" %%%ds" % width) % '-inf'
    elif math.isnan(x):
        return (" %%%ds" % width) % 'nan'
    
    s = ("{:+%d.%dg}"%(width,width)).format(x)
    if 'e' in s:
        tmp = s.split('e')
        return s[:width-len(tmp[1])-1] + 'e' + tmp[1]
    return s[0:width]
    #return "{:+6.3f}".format(min(9.999, max(0.001, a)))

def numpy_to_list(a):
    return list(np.asarray(a))
