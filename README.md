# Cenny's Keras Experiments

Naive experiments in Keras.

## Dependencies

* `git lfs`
* `conda`

## Install
* [Create and activate the `conda` environment](http://conda.pydata.org/docs/using/envs.html)
from `environment.yml`.

* In order for Jupyter outputs to be ignored by Git, once execute:
```git config --add include.path .gitconfig```

## Experiment 1: binary autoencoding
TODO

