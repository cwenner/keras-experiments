# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:expandtab:colorcolumn=80

import inspect
import math
import numpy as np
np.random.seed(1337)  # for reproducibility
import pandas as pd
pd.set_option('display.max_rows', 30)
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.path as path
import matplotlib.patches as patches

from keras import backend as K
from keras.callbacks import *
from keras.layers import *
from keras.models import *
from keras.preprocessing.text import *
from keras.utils import *
from keras.optimizers import *
from keras.layers.noise import *
from keras.layers.advanced_activations import *

from util import *
np.set_printoptions(formatter={'float': format_number})

