# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:expandtab:colorcolumn=80

import os, os.path
old_cwd = os.getcwd()
while 'notebooks' in os.getcwd():
    os.chdir('..')
import sys
rel_orig_cwd = os.path.relpath(old_cwd, os.getcwd())
sys.path += [rel_orig_cwd]
if rel_orig_cwd != 'notebooks':
    sys.path += ['notebooks']
